from toredis.commands import RedisCommandsMixin
from utils import task

for method in dir(RedisCommandsMixin):
    if method[0] != '_':
        setattr(RedisCommandsMixin, method,
                task(getattr(RedisCommandsMixin, method)))

from toredis import Redis

Redis.get_locked_connection = task(Redis.get_locked_connection)

redis = Redis()
