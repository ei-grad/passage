#!/usr/bin/env python2

from binascii import b2a_hex
import logging
import os

from ujson import dumps as json_dumps

from tornado import gen, web, ioloop

from utils import LOGIN_RE, LOGIN_RE_S
from models import (UserPw, Message, GlobalFeed, UserFeed, MentionFeed,
                    UserTagFeed, TagFeed)


class BaseHandler(web.RequestHandler):

    login_required = True
    resp = {
        'title': 'Passage',
        'result': None,
    }

    def initialize(self):
        self.messages = []

    def get_current_user(self):
        return self.get_secure_cookie('user')

    def prepare(self):
        if self.login_required and not self.get_current_user():
            self.redirect(self.get_login_url())

    def get(self):
        self.send_resp()

    def add_message(self, cls, text):
        self.messages.append((cls, text))

    def send_resp(self, **kwargs):
        result = dict(self.resp)
        result['messages'] = self.messages
        result.update(kwargs)
        self.format_response(result)

    def format_response(self, result):
        raise NotImplementedError()


class Json(object):
    def format_response(self, result):
        self.finish(json_dumps(result))


class Html(object):
    def format_response(self, result):
        result['user'] = self.get_current_user()
        self.render(self.template, **result)


class Login(BaseHandler):

    login_required = False
    resp = {
        'title': 'Login to Passage',
        'form': [
            ('Login', 'username', 'text', None),
            ('Password', 'password', 'password', None),
            (None, None, 'submit', 'Login')
        ],
        'result': None,
    }

    def get(self):
        if self.get_current_user():
            self.add_message('info', 'You are already authentcated.')
            self.send_resp(result='OK')
        else:
            self.send_resp()

    @web.asynchronous
    @gen.engine
    def post(self):

        username = self.get_argument('username')
        password = self.get_argument('password')

        good = yield UserPw(username).check(password)
        if not good:
            self.add_message("error", "Wrong username or password!")
            self.send_resp(result="ERROR")
            return

        self.set_secure_cookie('user', username)
        self.send_resp(result='OK')


class Logout(BaseHandler):

    resp = {
        'title': 'Logout',
        'result': None,
        'form': [(None, None, 'submit', 'Logout')]
    }

    def post(self):
        self.clear_all_cookies()
        self.send_resp(result='OK')


class Register(BaseHandler):

    login_required = False
    resp = {
        'title': 'Passage account registration',
        'result': None,
        'form': [
            ('User name', 'username', 'text', None),
            ('Password', 'password', 'password', None),
            (None, None, 'submit', 'Register')
        ],
    }

    @web.asynchronous
    @gen.engine
    def post(self):

        username = self.get_argument('username')
        password = self.get_argument('password')

        if LOGIN_RE.match(username) is None:
            self.add_message("error",
                "Login must begin with letter and contain only letters, "
                "digits, dashes and underscores."
            )
            self.send_resp(result="ERROR")
        else:
            good = yield UserPw(username).create(password)
            if good:
                self.set_secure_cookie('user', username)
                self.add_message("success", "Your account has been created.")
                self.send_resp(result='OK')
            else:
                self.add_message("error",
                                 "User %s already exists!" % username)
                self.send_resp(result="ERROR")


class MessageHandler(BaseHandler):

    resp = {
        'title': 'Post new message',
        'result': None,
        'form': [
            ('Text', 'text', 'text', None),
            (None, None, 'submit', 'Send')
        ],
    }

    @web.asynchronous
    @gen.engine
    def post(self):
        user = self.get_current_user()
        text = self.get_argument('text')
        message = yield Message.create(user, text)
        if message is not None:
            self.add_message('success', 'Message sent.')
            self.send_resp(result='OK', url='/%s/%s' % (user, message.msgid))
        else:
            raise web.HTTPError(500)


class FeedHandler(BaseHandler):

    login_required = False

    @web.asynchronous
    @gen.engine
    def get(self, *args, **kwargs):
        feed = self.model(*args, **kwargs)
        exists = yield feed.exists()
        if not exists:
            raise web.HTTPError(404)
        offset = self.get_argument('offset', 0)
        msgs = yield feed.get(offset=offset)
        self.send_resp(feed=[msg.to_json() for msg in msgs])

class GlobalFeedHandler(FeedHandler):
    model = GlobalFeed

class TagFeedHandler(FeedHandler):
    model = TagFeed

class UserTagFeedHandler(FeedHandler):
    model = UserTagFeed

class UserFeedHandler(FeedHandler):
    model = UserFeed

class MentionFeedHandler(FeedHandler):
    model = MentionFeed


class UserMessageHandler(BaseHandler):

    login_required = False

    @web.asynchronous
    @gen.engine
    def get(self, user, msgid):
        msg = yield Message(user=user, msgid=msgid).load()
        self.send_resp(message=msg.to_json())


urls = [
    (r'^/$', web.RedirectHandler, {"url": "/global.html"}),
]

handlers = [
    ('login', Login),
    ('logout', Logout),
    ('register', Register),
    ('message', MessageHandler),
    ('global', GlobalFeedHandler),
    ('tag/(?P<tag>.+)', TagFeedHandler),
    ('(?P<user>%s)/tag/(?P<tag>.+)' % LOGIN_RE_S, UserTagFeedHandler),
    ('(?P<user>%s)/feed' % LOGIN_RE_S, UserFeedHandler),
    ('(?P<user>%s)/mentions' % LOGIN_RE_S, MentionFeedHandler),
    (r'(?P<user>%s)/(?P<msgid>[0-9]+)' % LOGIN_RE_S, UserMessageHandler),
]

for url_re, handler in handlers:
    for fmt_mixin in [Json, Html]:
        hname = handler.__name__
        if hname.endswith('Handler'):
            hname = hname[:-len('Handler')]
        mname = fmt_mixin.__name__
        urls.append((
            '^/%s.%s$' % (url_re, mname.lower()),
            type(hname + mname, (fmt_mixin, handler), {
                'template': '%s.%s' % (hname.lower(), mname.lower())
            }),
            {}
        ))
    urls.append((
        '^/%s$' % url_re,
        type(hname + mname, (fmt_mixin, handler), {
            'template': '%s.%s' % (hname.lower(), mname.lower())
        }),
        {}
    ))


for r, h, d in urls:
    print(r)


application = web.Application(
    urls,
    template_path='templates',
    debug=True,
    login_url='/login.html',
    xsrf_cookies=True,
    cookie_secret=b2a_hex(os.urandom(32))
)

logging.basicConfig(level=logging.DEBUG)

if __name__ == "__main__":
    application.listen(8888)
    ioloop.IOLoop.instance().start()
