#!/usr/bin/env python

from functools import partial, wraps
import re
from threading import Thread

from tornado import gen, escape, ioloop, stack_context


LOGIN_RE_S = r'[a-zA-Z][a-zA-Z0-9_-]+'
LOGIN_RE = re.compile(LOGIN_RE_S)
MENTION_RE = re.compile(r'(\A|\s)@(?P<username>%s)' % LOGIN_RE_S)
TAG_RE = re.compile(r'(\A|\s)#(?P<tag>\w+)')


old_linkify = escape.linkify
def linkify(text, *args, **kwargs):
    text = old_linkify(text, *args, **kwargs)
    text = MENTION_RE.sub(r'\1@<a href="/\2/feed.html">\2</a>', text)
    return TAG_RE.sub(r'\1#<a href="/tag/\2.html">\2</a>', text)
escape.linkify = linkify


def task(method):
    """Seamless support for yield-based coroutines.

    If the keyword arg "callback" is given, we execute a callback-style
    call to the wrapped method. Otherwise, we return a tornado.gen.Task
    for use with tornado.gen.engine.
    """
    @wraps(method)
    def wrapper(*args, **kwargs):
        if "callback" not in kwargs:
            return gen.Task(method, *args, **kwargs)
        else:
            return method(*args, **kwargs)
    return wrapper


def inline_threaded(func):

    @wraps(func)
    def wrapper(*args, **kwargs):

        io_loop = ioloop.IOLoop.instance()

        # callback is expected to be in kwargs or to be the last argument
        if 'callback' in kwargs:
            orig_cb = kwargs.pop('callback')
        else:
            args, orig_cb = args[:-1], args[-1]
            assert callable(orig_cb)

        @stack_context.wrap
        def callback(result):
            @wraps(orig_cb)
            def thread_stopper():
                thread.join()
                orig_cb(result)
            io_loop.add_callback(thread_stopper)

        @stack_context.wrap
        def raise_exc(e):
            thread.join()
            raise e

        @wraps(func)
        def exception_handler():
            try:
                callback(func(*args, **kwargs))
            except Exception as e:
                io_loop.add_callback(partial(raise_exc, e))

        thread = Thread(target=exception_handler)
        thread.start()

    return wrapper


pairs = lambda x: zip(x[::2], x[1::2])
