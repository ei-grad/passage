#!/usr/bin/env python2
# coding: utf-8

import locale

from flask import abort, Flask, g, request

from psycopg2 import connect
from psycopg2.extensions import register_type, UNICODE

locale.setlocale(locale.LC_ALL, locale.getdefaultlocale())

conn = connect(database="gis")
register_type(UNICODE)

app = Flask(__name__)


@app.before_request
def before_request():
    g.db = conn.cursor()


@app.teardown_request
def teardown_request(exception):
    g.db.close()


def get_nearest_object(db, lat, lon):
    db.execute("""
SELECT l.osm_id, l.name
FROM planet_osm_line as l
INNER JOIN (
    SELECT ST_Transform(ST_GeomFromText(%s, 4326), 900913) AS way
) AS p
ON ST_DWithin(l.way, p.way, 100)
WHERE
    l.name IS NOT NULL
    AND l.highway IS NOT NULL
ORDER BY ST_Distance(p.way, l.way)
LIMIT 1
""", ['POINT(%s %s)' % (float(lon), float(lat))])
    if db.rowcount != 1:
        raise Exception()
    return db.fetchone()


def autotag(name):
    ret = "".join(i for i in name.split() if i[0].isupper())
    if not ret:
        raise Exception()
    return ret


@app.route('/id/<int:osm_id>')
def osmid_view(osm_id):
    g.db.execute('SELECT tag FROM passage_tags WHERE osm_id = %s', [osm_id])
    if g.db.rowcount == 1:
        return g.db.fetchone()[0]
    g.db.execute('SELECT name FROM planet_osm_line WHERE osm_id = %s', [osm_id])
    if g.db.rowcount == 1:
        return autotag(g.db.fetchone()[0])
    abort(404)


@app.route('/coords')
def tag_view():
    osm_id, name = get_nearest_object(g.db,
                                      request.args.get('lat'),
                                      request.args.get('lon'))
    g.db.execute("SELECT tag FROM passage_tags WHERE osm_id = %s", [osm_id])
    if g.db.rowcount != 1:
        return autotag(name)
    return g.db.fetchone()[0]


if __name__ == '__main__':
    app.run(debug=True)
