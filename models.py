#!/usr/bin/env python

import logging
from time import time

import bcrypt

from tornado import gen, web

from db import redis
from utils import task, inline_threaded, pairs, TAG_RE, MENTION_RE


hashpw = task(inline_threaded(bcrypt.hashpw))
logger = logging.getLogger(__name__)


class Model(object):

    def __init__(self, pk):
        self.pk = pk

    @property
    def key(self):
        return '%s:%s' % (self.__class__.__name__, self.pk)

    @task
    @gen.engine
    def exists(self, callback):
        res = yield redis.exists(self.key)
        callback(res == 1)


class UserPw(Model):

    @task
    @gen.engine
    def check(self, password, callback):
        ret = False
        exists = yield self.exists()
        if exists:
            pw_hash1 = yield redis.get(self.key)
            if pw_hash1:
                pw_hash2 = yield hashpw(password, pw_hash1)
                if pw_hash1 == pw_hash2:
                    ret = True
        callback(ret)

    @task
    @gen.engine
    def create(self, password, callback):
        r = yield redis.get_locked_connection()
        r.watch(self.key, callback=None)
        exists = yield r.exists(self.key)
        if exists == 1:
            r.unlock()
            callback(False)
        else:
            r.multi(callback=None)
            pw_hash = yield hashpw(password, bcrypt.gensalt())
            r.set(self.key, pw_hash, callback=None)
            results = yield r.execute()
            r.unlock()
            callback(results is not None)


class Feed(Model):

    @task
    @gen.engine
    def add_post(self, msgid, callback):
        redis.publish(self.key, msgid, callback=None)
        redis.lpush(self.key, [msgid], callback=callback)

    @task
    def get_ids(self, limit=20, offset=0, callback=None):
        assert callback is not None
        redis.lrange(self.key, offset, offset + limit, callback=callback)

    @task
    @gen.engine
    def get(self, limit=20, offset=0, callback=None):
        assert callback is not None
        msgids = yield self.get_ids()
        r = yield redis.get_locked_connection()
        r.multi(callback=None)
        for msgid in msgids:
            r.hgetall(Message(msgid).key, callback=None)
        results = yield r.execute()
        if results is None:
            logger.error('Nil EXEC result')
            raise web.HTTPError(500)
        assert len(results) == len(msgids)
        r.unlock()
        callback([Message(msgid, **dict(pairs(msg)))
                  for msgid, msg in zip(msgids, results)])


class GlobalFeed(Feed):

    def __init__(self, *args, **kwargs):
        super(GlobalFeed, self).__init__(None, *args, **kwargs)

    @property
    def key(self):
        return 'GlobalFeed'


class UserFeed(Feed):
    def __init__(self, user, *args, **kwargs):
        super(UserFeed, self).__init__(user, *args, **kwargs)


class UserTagFeed(Feed):

    def __init__(self, user, tag, *args, **kwargs):
        super(UserTagFeed, self).__init__(
            '%s:%s' % (user, tag),
            *args, **kwargs
        )


class TagFeed(Feed):
    def __init__(self, tag, *args, **kwargs):
        super(TagFeed, self).__init__(tag, *args, **kwargs)


class MentionFeed(UserFeed):
    pass


class Message(Model):

    def __init__(self, pk=None, user=None, msgid=None, text=None, location=None,
                 ts=None, *args, **kwargs):
        if pk is None:
            assert user is not None
            assert msgid is not None
            pk = '%s:%s' % (user, msgid)
        else:
            user, msgid = pk.split(':')
        super(Message, self).__init__(pk, *args, **kwargs)
        self.user = user
        self.msgid = msgid
        self.text = text
        self.location = location
        self.ts = ts

    @property
    def tags(self):
        return set(i.group('tag') for i in TAG_RE.finditer(self.text))

    @property
    def mentions(self):
        return set(i.group('username') for i in MENTION_RE.finditer(self.text))

    @property
    def url(self):
        return '/%s/%s' % (self.user, self.msgid)

    def to_json(self):
        return {
            'user': self.user,
            'msgid': self.msgid,
            'text': self.text,
            'tags': list(self.tags),
            'mentions': list(self.mentions),
            'ts': self.ts,
            'location': self.location,
            'url': self.url,
        }

    @classmethod
    @task
    @gen.engine
    def create(cls, user, text, location=None, callback=None):

        if location is not None:
            # XXX:
            pass

        msgid = yield redis.incr('MSGID')
        msg = cls(user=user, msgid=msgid, text=text, location=location,
                  ts=int(time()))

        GlobalFeed().add_post(msg.pk, callback=None)
        UserFeed(msg.user).add_post(msg.pk, callback=None)

        for tag in msg.tags:
            TagFeed(tag).add_post(msg.pk, callback=None)
            UserTagFeed(user, tag).add_post(msg.pk, callback=None)

        for mentioned_user in msg.mentions:
            MentionFeed(mentioned_user).add_post(msg.pk, callback=None)

        result = yield redis.hmset(msg.key, {
            'text': msg.text,
            'location': msg.location,
            'ts': msg.tx
        })
        assert result == 'OK'

        if callback is not None:
            callback(msg)

    @task
    @gen.engine
    def load(self, callback=None):
        result = yield redis.hgetall(self.key)
        d = dict(pairs(result))
        self.text = d['text']
        self.location = d['location']
        self.ts = int(d['ts'])
        callback(self)
